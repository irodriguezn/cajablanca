/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajablanca;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nachorod
 */
public class ej2Test {
    
    public ej2Test() {
    }

    /**
     * Test of notaFinal method, of class ej2.
     */
    @Test
    public void testNotaFinal() {
        System.out.println("notaFinal");
        assertEquals("4.0", ej2.notaFinal(4, "No Apto", 7));
        assertEquals("4.0", ej2.notaFinal(4, "Apto", 7));
        assertEquals("8.5", ej2.notaFinal(4.5F, "Apto", 4));
        assertEquals("Matrícula de Honor", ej2.notaFinal(4.5f, "Apto", 7));
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
