/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajablanca;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nachorod
 */
public class ej3Test {
    
    public ej3Test() {
    }

    /**
     * Test of buscarEn method, of class ej3.
     */
    @Test
    public void testBuscarEn() {
        assertEquals(0, ej3.buscarEn("", 'a'));
        assertEquals(0, ej3.buscarEn("a", 'b'));
        assertEquals(1, ej3.buscarEn("a", 'a'));
        assertEquals(1, ej3.buscarEn("al", 'a'));
    }
    
}
