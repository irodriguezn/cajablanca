/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajablanca;

/**
 *
 * @author lliurex
 */
public class ej2 {
    public static String notaFinal(float notaTeoria, String notaPractica, float notaTrabajos) {
        float notaFinal;
        String resultado="";
        
        if (notaPractica.equals("No Apto")) {
            notaFinal=4;
        } else {
            if (notaTeoria<4.5) {
                notaFinal=notaTeoria;
            } else {
                notaFinal=notaTeoria+notaTrabajos;
            }
        }
        
        if (notaFinal>10) {
            resultado="Matrícula de Honor";
        } else {
            resultado=notaFinal + "";
        }
        
        return resultado;
    }
}


