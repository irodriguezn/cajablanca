/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cajablanca;

/**
 *
 * @author nachorod
 */
public class Pelicula {
    public static enum Formato {DVD, VHS};
    
    private String idPelicula;
    private String titulo;
    private Formato formato;
    private boolean estaAlquilado; // True Alquilado y False no

    public Pelicula(String idPelicula, String titulo, Formato formato, boolean estaAlquilado) {
        this.idPelicula = idPelicula;
        this.titulo = titulo;
        this.formato = formato;
        this.estaAlquilado = estaAlquilado;
    }

    public Formato getFormato() {
        return formato;
    }

    public void setFormato(Formato formato) {
        this.formato = formato;
    }


    public String getIdPelicula() {
        return idPelicula;
    }

    public void setIdPelicula(String idPelicula) {
        this.idPelicula = idPelicula;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public boolean isEstaAlquilado() {
        return estaAlquilado;
    }

    public void setEstaAlquilado(boolean estaAlquilado) {
        this.estaAlquilado = estaAlquilado;
    }

    @Override
    public String toString() {
        return "Pelicula{" + "idPelicula=" + idPelicula + ", titulo=" + titulo + ", formato=" + formato + ", estaAlquilado=" + estaAlquilado + '}';
    }

    
 
}
