/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cajablanca;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author nachorod
 */
public class CalcularMedia1 {
    public static void main(String[] args) throws IOException {
        int numValores=0;
        int suma=0;
        int valor=0;
        String digitos="0123456789";
        String linea="";
        boolean noNumerico=false;
        boolean hayError=false;
        try {
            FileReader fr=new FileReader("numeros4.txt");
            BufferedReader br=new BufferedReader(fr);
            linea=br.readLine();
            if (linea!=null) {
                valor=Integer.parseInt(linea);
            }
            while (valor!=0 && linea!=null) {
                numValores++;
                suma+=valor;
                linea=br.readLine();
                if (linea!=null) {
                    valor=Integer.parseInt(linea);
                }
            }
            br.close();
            fr.close();
        } catch (FileNotFoundException ex) {
            System.out.println("No se encuentra el archivo");
            hayError=true;
        } catch (NumberFormatException ex) {
            char letra;
            for (int i=0; i<linea.length();i++) {
                letra=linea.charAt(i);
                if (digitos.indexOf(letra)==-1) {
                    noNumerico=true;
                    break;
                }
            }
            if (noNumerico) {
                System.out.println("El archivo contiene caracteres no numéricos");
            } else {
                System.out.println("El archivo contiene números demasiado grandes");
            }
            hayError=true;
        } catch (Exception e) {
            System.out.println("Error leyendo números del archivo");
            hayError=true;
        }
        if (!hayError) {
            if (numValores>0) {
                double media=(double)suma/numValores;
                System.out.println("Se leyeron " + numValores + " números y la media es: " + media);
            } else {
                System.out.println("No se ha leído ningún número");
            }
        }
    }
}
