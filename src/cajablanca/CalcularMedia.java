/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cajablanca;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nachorod
 */
public class CalcularMedia {
    public static void main(String[] args) throws IOException {
        int numValores=0;
        int suma=0;
        int valor=0;
        char valorCrudo=0;
        try {
            FileReader fr=new FileReader("numeros.txt");
            valorCrudo=(char)fr.read();
            valor=Integer.parseInt(valorCrudo+"");
            while (valor!=-1 && valor!=0) {
                numValores++;
                suma+=valor;
                valorCrudo=(char)fr.read();
                valor=Integer.parseInt(valorCrudo+"");
            }
            fr.read();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CalcularMedia.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (numValores>0) {
            double media=(double)suma/numValores;
            System.out.println("Se leyeron " + numValores + " números y la media es: " + media);
        } else {
            System.out.println("No se ha leído ningún número");
        }
        
    }
}
