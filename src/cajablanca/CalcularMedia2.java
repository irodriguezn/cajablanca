/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cajablanca;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 *
 * @author nachorod
 */
public class CalcularMedia2 {
    public static void main(String[] args) throws IOException {
        int numValores=0;
        int suma=0;
        int valor=0;
        String linea;
        
        try {
            FileReader fr=new FileReader("numeros2.txt");
            BufferedReader br=new BufferedReader(fr);
            linea=br.readLine();
            StringTokenizer t=new StringTokenizer(linea);
            
            if (t.hasMoreTokens()) {
                valor=Integer.parseInt(t.nextToken());
            }
            while (valor!=0) {
                numValores++;
                suma+=valor;
                //linea=br.readLine();
                if (t.hasMoreTokens()) {
                    valor=Integer.parseInt(t.nextToken());
                } else {
                    t=new StringTokenizer(br.readLine());
                    if (t.hasMoreTokens()) {
                        valor=Integer.parseInt(t.nextToken());
                    } else {
                        valor=0;
                    }
                }
            }
            br.close();
            fr.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Fichero no encontrado");
        }
        if (numValores>0) {
            double media=(double)suma/numValores;
            System.out.println("Se leyeron " + numValores + " números y la media es: " + media);
        } else {
            System.out.println("No se ha leído ningún número");
        }
        
    }
}
