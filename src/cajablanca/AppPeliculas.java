/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cajablanca;
import static cajablanca.Pelicula.Formato;

/**
 *
 * @author nachorod
 */
public class AppPeliculas {
    public static void main(String[] args) {
        Pelicula pel1=new Pelicula("0001", "Star Wars", Formato.DVD, true);
        Pelicula pel2=new Pelicula("0002", "Star Wars", Formato.VHS, true);
        Pelicula pel3=new Pelicula("0003", "Matrix", Formato.DVD, true);
        Pelicula pel4=new Pelicula("0004", "Matrix", Formato.VHS, false);
        Videoteca vid1=new Videoteca();
        vid1.añadirPelicula(pel1);
        vid1.añadirPelicula(pel2);
        vid1.añadirPelicula(pel3);
        vid1.añadirPelicula(pel4);
        vid1.mostrarPeliculas();
    }
}
