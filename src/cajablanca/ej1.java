/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cajablanca;

/**
 *
 * @author lliurex
 */
public class ej1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    // Dada una cadena devuelve si existe una letra dentro de esta que sea
    // mayor que la letra pasada como parámetro
    public static boolean hayCarMayorEnCadena (String cadena, char letra) {
        boolean encontrado=false;
        int longitud=cadena.length();
        int i=0;
        while (!encontrado && i<longitud) {
            if (cadena.charAt(i)>letra) {
                encontrado=true;
            }
            i++;
        }
        return encontrado;
    }
    
}
